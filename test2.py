import threading

from multiprocessing import Queue

import time
from random import randint


def worker(i):
    """thread worker function"""
    print 'Worker {}'.format(i)
    time.sleep(randint(0, 1))
    queue.put(i)
    return


if __name__ == '__main__':
    global queue
    queue = Queue()
    threads = []
    for i in range(5):
        t = threading.Thread(target=worker, args=(i,))
        threads.append(t)
        t.start()
        print queue.get_nowait()

    for t in threads:
        t.start()