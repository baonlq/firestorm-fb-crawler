import requests
from pymongo import MongoClient


def importer():
    mongodb_uri = "mongodb://103.68.82.34:3366/analytics"
    connection = MongoClient(mongodb_uri, connect=False)
    db = connection.analytics
    f = open('token-da-gd-170-token.txt', 'r')
    for line in f.readlines():
        line = line.strip()
        print 'inserting ', line
        token = {
            'start': None,
            'count': 0,
            'token': line
        }
        db['tokens'].insert_one(token)


def _is_alive(token):
    graph_url = 'https://graph.facebook.com/'
    graph_ver = 'v2.9'
    root_url = graph_url + graph_ver + '/'
    url_within_token = root_url + '/me?fields=id,name' + "&access_token=" + token
    r = requests.get(url_within_token, headers={
        'Connection': 'close'
    })
    requests_result = r.json()
    # print requests_result
    if 'error' in requests_result:
        # print 'token dead!!'
        return False
    # print 'I am alive'
    return True


def check():
    f = open('token-da-gd-170-token.txt', 'r')
    alive_tokens = 0
    count = 0
    for line in f.readlines():
        line = line.strip()
        if _is_alive(line):
            alive_tokens += 1
        count += 1
    print 'alive {}/{}'.format(alive_tokens, count)


if __name__ == '__main__':
    # check()
    importer()
