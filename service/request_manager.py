from datetime import datetime

import requests
import time

from service.data_handler import DataHandler
from service.logger import b_log


class RequestManager:
    tokens = []
    graph_url = 'https://graph.facebook.com/'
    graph_ver = 'v2.9'

    def __init__(self):
        self.root_url = self.graph_url + self.graph_ver + '/'
        self.session = requests.session()
        self.dtHandler = DataHandler()
        self.conn = self.dtHandler.get_connection()
        self.db = self.dtHandler.get_database()
        self.clt_token = self.db.get_collection('tokens')
        self._get_tokens()

    def _get_tokens(self):
        try:
            self.tokens = self.clt_token.find().sort('count', 1)
            # print self.tokens
            return self.tokens
        except Exception as e:
            b_log('Exception: {}'.format(e))

    @staticmethod
    def _is_limit_reached(token):
        if 19000 < token.get('count'):
            return False
        return True

    @staticmethod
    def _can_be_used(token):
        if 'has_error' in token:
            return False
        return True

    # return (days, hours, minutes)
    @staticmethod
    def _get_hours_from_timedelta(time):
        return time.days, time.seconds // 3600, (time.seconds // 60) % 60

    def _is_alive(self, token):
        url_within_token = self.root_url + '/me?fields=id,name' + "&access_token=" + token
        r = requests.get(url_within_token, headers={
            'Connection': 'close'
        })
        requests_result = r.json()
        if 'error' in requests_result:
            print 'token dead!!'
            return False
        return True

    def _check_available(self, token):
        try:
            current_time = datetime.now()
            if not self._is_alive(token['token']):
                return False
            # print 'currenttime {}'.format(current_time)
            # the token is not used before
            if token.get('start') is None:
                self.clt_token.update({'_id': token['_id']}, {'$set': {'start': current_time, 'count': 0}},
                                      upsert=False)
                return True
            else:
                start_time = token['start']
                # print 'start_time: {}'.format(start_time)
                # print 'sub: {}'.format(current_time - start_time)
                sub_time = self._get_hours_from_timedelta(current_time - start_time)
                days = sub_time[0]
                hours = sub_time[1]
                minutes = sub_time[2]
                # print 'days {}, hours {}, minutes {}'.format(days, hours, minutes)
                if days > 0 or hours >= 1:
                    # reset token usage
                    self.clt_token.update({'_id': token['_id']}, {'$set': {'start': current_time, 'count': 0}},
                                          upsert=False)
                    return True
                elif hours == 0 and minutes < 60:
                    if not self._is_limit_reached(token) and not self._can_be_used(token):
                        b_log('token {} reach limit or cannot be used'.format(token['token']))
                        return False
                    else:
                        return True
            return False
        except Exception as e:
            b_log('_check_available ' + str(e))

    def _get_available_token(self):
        for token in self._get_tokens():
            # print token
            if self._check_available(token):
                return self.clt_token.find_one({'_id': token.get('_id')})
                # break
        return None

    def request_token(self):
        available_token = self._get_available_token()
        # print 'available_token: {}'.format(available_token)
        if available_token is not None:
            return available_token
        else:
            b_log("Everything is overflow")
            return None

    @staticmethod
    def _check_response(response):
        if 'error' in response:
            return False
        else:
            return True

    def make_requests(self, path, flag):
        try:
            token = self.request_token()
            headers = {
                'Connection': 'close'
            }

            if token is not None:
                if flag == 'path':
                    url_within_token = self.root_url + path + "&access_token=" + token.get('token')
                else:
                    url_within_token = path

                execution_start_time = time.time()
                b_log('start crawling path {}'.format(url_within_token))
                # time.sleep(0.5)
                r = requests.get(url_within_token, headers=headers)
                # b_log('request result: {}'.format(requests_result.text.encode('utf-8')))
                requests_result = r.json()
                cost_time = time.time() - execution_start_time
                b_log('Request completed.Time Cost: ' + str(cost_time))

                if self._check_response(requests_result):
                    self.clt_token.update({'_id': token.get('_id')}, {'$inc': {'count': 1}}, upsert=False)
                    return requests_result
                else:
                    if 'type' in requests_result['error']:
                        if requests_result['error']['type'] == 'GraphMethodException':
                            b_log(requests_result.get('error'))
                        elif requests_result['error']['type'] == 'OAuthException':
                            if requests_result['error']['code'] == 190 and requests_result['error'][
                                'error_subcode'] == 490:
                                # save error to db
                                self.clt_token.update({'_id': token.get('_id')},
                                                      {'$set': {'has_error': True,
                                                                'error': requests_result.get('error')}},
                                                      upsert=False)
                                b_log(requests_result['error']['message'])
                            elif requests_result['error']['code'] == 100:
                                b_log('RequestManagerWarning: This uid is page')
                                return 'type_page'
                    return None
            else:
                return None
        except Exception as e:
            b_log('request_manager {}'.format(e))
