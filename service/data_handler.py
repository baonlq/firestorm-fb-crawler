from pymongo import MongoClient

from service.configs import server_ip


class DataHandler:
    conn = None
    db = None

    def __init__(self):
        # self.mongodb_uri = "mongodb://bean_mongo:V38T2B5?K2gSh*Q1@{}:3366/analytics".format(server_ip)
        # self.mongodb_uri = "mongodb://bean_mongo:V38T2B5?K2gSh*Q1@173.208.199.99:3366/analytics"
        self.mongodb_uri = "mongodb://103.68.82.34:3366/analytics"

    def get_connection(self):
        if self.conn is None:
            # Mongodb
            connection = MongoClient(self.mongodb_uri, connect=False)
            # testing with 'test_database'
            # DataHandler.db = connection.facebook_bigdata
            # prod
            self.conn = connection
        return self.conn

    def get_database(self):
        if self.db is None:
            return self.conn.analytics
        return self.db

    def get_collection(self, name):
        return self.db[name]
