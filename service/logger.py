from datetime import datetime
from multiprocessing import current_process


def b_log(message):
    print('{}: "{}" -- {}'.format(datetime.strftime(datetime.now(), '%Y-%m-%d %H:%M:%S'), current_process().name, message))
