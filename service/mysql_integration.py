import MySQLdb
import time

import datetime

from service.configs import server_ip
from service.logger import b_log


class MysqlIntegration:
    def __init__(self):
        self.db = MySQLdb.connect(server_ip, "r_user", "newworld", "fb_report", charset='utf8')
        # self.db = MySQLdb.connect("localhost", "root", "newworld", "fb_report", charset='utf8')
        # self.db = MySQLdb.connect("localhost", "root", "", "fb_report", charset='utf8')

    def create_report(self):
        cursor = self.db.cursor()
        new_report = {
            'name': 'report-{}'.format(str(time.time()).split('.')[0]),
            'created_at': datetime.datetime.now().strftime('%m/%d/%Y %I:%M%p')
        }
        sql = "INSERT INTO `reports` (`report_name`, `created_at`) VALUES ('{}', '{}');".format(new_report['name'],
                                                                                                new_report[
                                                                                                    'created_at'])
        try:
            # Execute the SQL command
            cursor.execute(sql)
            # Commit your changes in the database
            self.db.commit()
            report_id = cursor.lastrowid
            # print('report_id: {}'.format(report_id))
            return report_id
        except Exception as e:
            b_log('Exception create_report: {}'.format(e))
            # Rollback in case there is any error
            self.db.rollback()
            return None

    def get_all_pending_report(self):
        cursor = self.db.cursor()
        sql = "SELECT * FROM `reports` WHERE `status` = '{}'".format('pending')

        try:
            # Execute the SQL command
            cursor.execute(sql)
            # Fetch all the rows in a list of lists.
            results = cursor.fetchall()

            reports = []
            for row in results:
                report = {
                    'id': row[0],
                    'report_name': row[3],
                    'created_at': row[2],
                    'status': row[4],
                    'attachment': row[1],
                }
                # print(report)
                reports.append(report)

            return reports
        except Exception as e:
            b_log('Exception find_location_report: {}'.format(e))
            return []

    def update_report_status(self, status, report_id):
        cursor = self.db.cursor()
        sql = "UPDATE `reports` SET `status` = '{}' WHERE `id` = {}".format(status, report_id)
        try:
            # Execute the SQL command
            cursor.execute(sql)
            # Commit your changes in the database
            self.db.commit()
        except Exception as e:
            b_log('Exception update_report_status {}'.format(e))
            # Rollback in case there is any error
            self.db.rollback()

    def find_location_report(self, report_id, location_id):
        cursor = self.db.cursor()
        sql = "SELECT * FROM `location` WHERE `location_id` = '{}' AND `report_id` = '{}'".format(location_id,
                                                                                                  report_id)
        try:
            # Execute the SQL command
            cursor.execute(sql)
            # Fetch all the rows in a list of lists.
            results = cursor.fetchall()
            # print(results)

            location_report = None
            for row in results:
                location_report = {
                    'id': row[0],
                    'location_id': row[1],
                    'location_name': row[2],
                    'count': row[3],
                    'report_id': row[4],
                }
                # print(location_report)

            return location_report
        except Exception as e:
            b_log('Exception find_location_report: {}'.format(e))
            return None

    def update_count_report(self, table, record_id):
        cursor = self.db.cursor()
        sql = "UPDATE `{}` SET `count` = `count` + 1 WHERE `id` = {}".format(table, record_id)
        try:
            # Execute the SQL command
            cursor.execute(sql)
            # Commit your changes in the database
            self.db.commit()
        except Exception as e:
            b_log('Exception update_count_report {}'.format(e))
            # Rollback in case there is any error
            self.db.rollback()

    def insert_location_report(self, record):
        cursor = self.db.cursor()
        sql = "INSERT INTO `location` (`location_id`, `location_name`, `count`, `report_id`) VALUES ('{}', '{}', {}, {});".format(
            record['location_id'], record['location_name'], record['count'], record['report_id'])
        try:
            # Execute the SQL command
            cursor.execute(sql)
            location_id = cursor.lastrowid
            # Commit your changes in the database
            self.db.commit()
            return location_id
        except Exception as e:
            b_log('Exception insert_location_report: {}'.format(e))
            # Rollback in case there is any error
            self.db.rollback()
            return None

    def add_location_details(self, location_report_id, uid):
        cursor = self.db.cursor()
        sql = "INSERT INTO `fb_report`.`location_details` (`lid`, `uid`) VALUES ({}, {});".format(location_report_id,
                                                                                                  uid)
        try:
            # Execute the SQL command
            cursor.execute(sql)
            # Commit your changes in the database
            self.db.commit()
        except Exception as e:
            b_log('Exception insert_location_report: {}'.format(e))
            # Rollback in case there is any error
            self.db.rollback()

    def find_age_report(self, age, report_id):
        cursor = self.db.cursor()
        sql = "SELECT * FROM `age` WHERE `age` = '{}' AND `report_id` = '{}'".format(age,
                                                                                     report_id)
        try:
            # Execute the SQL command
            cursor.execute(sql)
            # Fetch all the rows in a list of lists.
            results = cursor.fetchall()

            age_report = None
            for row in results:
                age_report = {
                    'id': row[0],
                    'age': row[1],
                    'report_id': row[2],
                    'count': row[3],
                }
                # print(age_report)

            return age_report
        except Exception as e:
            b_log('Exception find_age_report: {}'.format(e))
            return None

    def insert_age_report(self, record):
        cursor = self.db.cursor()
        sql = "INSERT INTO `age` (`age`, `report_id`, `count`) VALUES ('{}', '{}', {});".format(
            record['age'], record['report_id'], record['count'])
        try:
            # Execute the SQL command
            cursor.execute(sql)
            age_id = cursor.lastrowid
            # Commit your changes in the database
            self.db.commit()
            return age_id
        except Exception as e:
            b_log('Exception insert_age_report: {}'.format(e))
            # Rollback in case there is any error
            self.db.rollback()
            return None

    def add_age_details(self, age_id, uid):
        cursor = self.db.cursor()
        sql = "INSERT INTO `age_details` (`age_id`, `uid`) VALUES ({}, {});".format(age_id,
                                                                                    uid)
        try:
            # Execute the SQL command
            cursor.execute(sql)
            # Commit your changes in the database
            self.db.commit()
        except Exception as e:
            b_log('Exception add_age_details: {}'.format(e))
            # Rollback in case there is any error
            self.db.rollback()

    def find_gender_report(self, gender, report_id):
        cursor = self.db.cursor()
        sql = "SELECT * FROM `gender` WHERE `gender` = '{}' AND `report_id` = '{}'".format(gender,
                                                                                           report_id)
        try:
            # Execute the SQL command
            cursor.execute(sql)
            # Fetch all the rows in a list of lists.
            results = cursor.fetchall()

            gender_report = None
            for row in results:
                gender_report = {
                    'id': row[0],
                    'gender': row[1],
                    'report_id': row[3],
                    'count': row[2],
                }
                # print(gender_report)

            return gender_report
        except Exception as e:
            b_log('Exception find_gender_report: {}'.format(e))
            return None

    def insert_gender_report(self, record):
        cursor = self.db.cursor()
        sql = "INSERT INTO `gender` (`gender`, `count`, `report_id`) VALUES ('{}', '{}', {});".format(
            record['gender'], record['count'], record['report_id'])
        try:
            # Execute the SQL command
            cursor.execute(sql)
            gender_id = cursor.lastrowid
            # Commit your changes in the database
            self.db.commit()
            return gender_id
        except Exception as e:
            b_log('Exception insert_gender_report: {}'.format(e))
            # Rollback in case there is any error
            self.db.rollback()
            return None

    def add_gender_details(self, gender_id, uid):
        cursor = self.db.cursor()
        sql = "INSERT INTO `gender_details` (`gender_id`, `uid`) VALUES ({}, {});".format(gender_id,
                                                                                          uid)
        try:
            # Execute the SQL command
            cursor.execute(sql)
            # Commit your changes in the database
            self.db.commit()
        except Exception as e:
            b_log('Exception add_gender_details: {}'.format(e))
            # Rollback in case there is any error
            self.db.rollback()

    def find_liked_page_report(self, page, report_id):
        cursor = self.db.cursor()
        sql = "SELECT * FROM `pages` WHERE `page` = '{}' AND `report_id` = '{}'".format(page,
                                                                                        report_id)
        try:
            # Execute the SQL command
            cursor.execute(sql)
            # Fetch all the rows in a list of lists.
            results = cursor.fetchall()

            page_report = None
            for row in results:
                page_report = {
                    'id': row[0],
                    'page': row[1],
                    'count': row[2],
                    'report_id': row[3],
                }
                # print(page_report)

            return page_report
        except Exception as e:
            b_log('Exception find_liked_page_report: {}'.format(e))
            return None

    def insert_liked_page_report(self, record):
        cursor = self.db.cursor()
        # sql = "INSERT INTO `pages` (`page`, `page_name`, `count`, `report_id`) VALUES ('{}', '{}', {}, {});".format(
        #     record['page'], record['page_name'].encode('utf-8'), record['count'], record['report_id'])

        sql = "INSERT INTO `pages` (`page`, `count`, `report_id`, `page_name`) VALUES (%s, %s, %s, %s)"
        # print(sql)
        try:
            # Execute the SQL command
            cursor.execute(sql,
                           (record['page'], record['count'], record['report_id'], record['page_name'].encode('utf-8')))
            # Commit your changes in the database
            page_id = cursor.lastrowid
            self.db.commit()
            return page_id
        except Exception as e:
            b_log('Exception insert_liked_page_report: {}'.format(e))
            # Rollback in case there is any error
            self.db.rollback()
            return None

    def add_page_details(self, page_id, uid):
        cursor = self.db.cursor()
        sql = "INSERT INTO `page_details` (`page_id`, `uid`) VALUES ({}, {});".format(page_id,
                                                                                      uid)
        try:
            # Execute the SQL command
            cursor.execute(sql)
            # Commit your changes in the database
            self.db.commit()
        except Exception as e:
            b_log('Exception add_page_details: {}'.format(e))
            # Rollback in case there is any error
            self.db.rollback()

    def find_relationship_status_report(self, relationship_status, report_id):
        cursor = self.db.cursor()
        sql = "SELECT * FROM `relationship_status` WHERE `relationship_status` = '{}' AND `report_id` = {}".format(
            relationship_status,
            report_id)
        try:
            # Execute the SQL command
            cursor.execute(sql)
            # Fetch all the rows in a list of lists.
            results = cursor.fetchall()

            relationship_status_report = None
            for row in results:
                # print(row)
                relationship_status_report = {
                    'id': row[0],
                    'relationship_status': row[1],
                    'count': row[2],
                    'report_id': row[3],
                }
                # print(relationship_status_report)

            return relationship_status_report
        except Exception as e:
            b_log('Exception find_relationship_status_report: {}'.format(e))
            return None

    def insert_relationship_status_report(self, record):
        cursor = self.db.cursor()
        sql = "INSERT INTO `relationship_status` (`relationship_status`, `count`, `report_id`) VALUES ('{}', '{}', {});".format(
            record['relationship_status'], record['count'], record['report_id'])
        try:
            # Execute the SQL command
            cursor.execute(sql)
            # Commit your changes in the database
            page_id = cursor.lastrowid
            self.db.commit()
            return page_id
        except Exception as e:
            b_log('Exception insert_relationship_status_report: {}'.format(e))
            # Rollback in case there is any error
            self.db.rollback()
            return None

    def add_relationship_details(self, relationship_id, uid):
        # print 'rela {} {}'.format(relationship_id, uid)
        cursor = self.db.cursor()
        sql = "INSERT INTO `relationship_details` (`relationship_id`, `uid`) VALUES ({}, {});".format(relationship_id,
                                                                                                      uid)
        # print 'rela sql {}'.format(sql)
        try:
            # Execute the SQL command
            cursor.execute(sql)
            # Commit your changes in the database
            self.db.commit()
        except Exception as e:
            b_log('Exception add_relationship_details: {}'.format(e))
            # Rollback in case there is any error
            self.db.rollback()
