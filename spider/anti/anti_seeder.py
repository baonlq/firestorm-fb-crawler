# coding=utf-8
import sys
import time
from splinter import Browser
from splinter.exceptions import ElementDoesNotExist

reload(sys)
sys.setdefaultencoding('utf8')


def is_numeric(c):
    try:
        val = int(c)
        return True
    except ValueError:
        # print("That's not an int!")
        return False


class AntiSeeder:
    browser = None

    def __init__(self, username, password):
        self.scanned_uid_count = 0
        self.username = username
        self.password = password

    def create_browser(self):
        if self.browser is None:
            self.browser = Browser('chrome')
            self.browser.driver.set_window_size(360, 640)
        return self.browser

    def switch_lang(self):
        try:
            if self.browser.is_text_present('English (UK)'):
                self.browser.find_link_by_text('English (UK)').first.click()
            elif self.browser.is_text_present('English (US)'):
                self.browser.find_link_by_text('English (US)').first.click()
        except ElementDoesNotExist:
            print 'no elements could be found'

    def _security_check(self):
        try:
            captcha_div = self.browser.find_by_xpath('//*[@id="captcha"]/h3').first
            if captcha_div.text == 'Security Check' or captcha_div.text == 'Kiểm tra Bảo mật'.encode('utf-8'):
                print 'Security Check'
                time.sleep(30)
        except Exception as e:
            print e

    def cross_single_signin_page(self):
        try:
            time.sleep(3)
            single_sign_in = self.browser.find_by_css('h3').first.text
            if 'Lưu mật khẩu'.decode('utf-8') in single_sign_in or 'Đăng nhập bằng một lần nhấn'.decode(
                    'utf-8') in single_sign_in:
                btn = self.browser.find_by_value('OK')
                btn.click()
        except ElementDoesNotExist:
            print 'no elements could be found'

    def cross_getting_started_page(self):
        try:
            if self.browser.is_text_present('Lời mời kết bạn'.decode('utf-8')):
                self.browser.find_by_xpath('//*[@id="nux-nav-button"]').click()

            time.sleep(1)
            if self.browser.is_text_present('Những người bạn có thể biết'.decode('utf-8')):
                self.browser.find_by_xpath('//*[@id="nux-nav-button"]').click()

            time.sleep(1)
            if self.browser.is_text_present('Tìm kiếm bạn bè'.decode('utf-8')):
                self.browser.find_by_xpath('//*[@id="nux-nav-button"]').click()
        except Exception as e:
            print 'something went wrong : {}'.format(e)

    def _input_password(self, password):
        time.sleep(0.5)
        try:
            for key in self.browser.type('pass', password, slowly=True):
                time.sleep(0.05)
        except Exception as e:
            print 'password input is not visible'
            try:
                self.browser.find_by_value('Continue'.decode('utf-8')).click()
            except Exception as e:
                print e
                try:
                    self.browser.find_by_value('Next'.decode('utf-8')).click()
                except Exception as e:
                    print e
                    try:
                        self.browser.find_by_value('Log In'.decode('utf-8')).click()
                    except Exception as e:
                        self.browser.find_by_value('Continue to Log In'.decode('utf-8')).click()
            print 'retrying...'
            self._input_password(password)

    def _do_login(self, username, password):
        self.switch_lang()

        for key in self.browser.type('email', username, slowly=True):
            time.sleep(0.05)

        self._input_password(password)

        time.sleep(2)
        login_btn = self.browser.find_by_name('login')
        login_btn.click()
        self.cross_single_signin_page()
        self.cross_getting_started_page()
        self._security_check()

    def _do_scanning(self, uid):
        try:
            self.browser.visit('https://m.facebook.com/' + uid)
            time.sleep(1)
            self._security_check()
            following = self.browser.find_by_xpath(
                '//*[@id="profile_intro_card"]/div/div/div')
            for row in following:
                # print 'row {}'.format(row.text.decode('utf-8'))
                if 'người'.decode('utf-8') in row.text.decode('utf-8'):
                    n_follows_str = ''
                    # print row.text.decode('utf-8')
                    for c in row.text.decode('utf-8'):
                        # print c
                        if is_numeric(c):
                            n_follows_str += c
                    try:
                        self.scanned_uid_count += 1
                        return int(n_follows_str) > 20
                    except:
                        self.scanned_uid_count -= 1
                        return False
        except Exception as e:
            print e

    def log_in(self):
        self.browser.visit('https://m.facebook.com/login.php')
        self._do_login(self.username, self.password)

    def scan_uid(self, uid):
        try:
            while 'https://m.facebook.com/home.php' not in self.browser.driver.current_url and self.scanned_uid_count == 0:
                time.sleep(0.5)
                # print 'tao dang doi'
            else:
                time.sleep(2)
                return self._do_scanning(uid)
        except Exception as e:
            print e

    def reset(self):
        self.scanned_uid_count = 0