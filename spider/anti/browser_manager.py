import shared
from spider.anti.anti_seeder import AntiSeeder


class _Singleton(type):
    """ A metaclass that creates a Singleton base class when called. """
    _instances = {}

    def __call__(cls, *args, **kwargs):
        if cls not in cls._instances:
            cls._instances[cls] = super(_Singleton, cls).__call__(*args, **kwargs)
        return cls._instances[cls]


class Singleton(_Singleton('SingletonMeta', (object,), {})):
    pass


class BrowserManager(Singleton):
    scanners = []

    def create_browsers(self):
        account_file = open('spider/anti/account', 'r')

        account = {}
        for line in account_file.readlines():
            line = line.strip()
            data = line.split('|')
            scanner = AntiSeeder(username=data[0], password=data[1])
            scanner.create_browser()
            self.scanners.append({
                'scanner': scanner,
                'status': 'free'
            })
        print 'created {} scanners'.format(BrowserManager.scanners)

    def get_scanners(self):
        return BrowserManager.scanners
