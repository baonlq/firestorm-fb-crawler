from Queue import Queue

import time

import shared
from spider.anti.browser_manager import BrowserManager
from spider.worker.anti_seeder_task import AntiSeederTask


class ScanningExecutor:
    def __init__(self):
        self.uid_list = []
        self.m = BrowserManager()
        self.results = Queue()

    def add_uid(self, uid):
        self.uid_list.append(uid)

    def start_scan(self):
        # print 'start_scan {}'.format(shared.scanners)
        i = 0
        for uid in self.uid_list:
            # queue = Queue()
            t = AntiSeederTask(args=(uid,))
            try:
                t.start()
            except Exception as e:
                time.sleep(0.5)
                t.start()
