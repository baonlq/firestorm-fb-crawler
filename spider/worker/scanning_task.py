import time

import shared
from spider.anti.browser_manager import BrowserManager


class ScanningTask:
    def __init__(self, uid, scanners):
        self.uid = uid
        self.g_scanners = scanners
        self.m = BrowserManager()

    def count_free_worker(self):
        c = 0
        print 'scanners: {}'.format(self.g_scanners)
        for scanner in self.g_scanners:
            if scanner['status'] == 'free':
                c += 1
        return c

    def scan(self, uid):
        print 'scanning {}'.format(uid)
        for worker in self.g_scanners:
            if worker['status'] == 'free':
                worker['status'] = 'busy'
                res = worker['scanner'].scan_uid(uid=uid)
                worker['status'] = 'free'
                return res

    def start_process(self):
        print 'scanners: {}'.format(self.g_scanners)
        print 'start with {}'.format(self.uid)
        print '{} free workers'.format(self.count_free_worker())
        while self.count_free_worker() == 0:
            # print 'waiting for free worker'
            time.sleep(0.5)
        else:
            # self.m.scan(uid)
            return self.scan(self.uid)
