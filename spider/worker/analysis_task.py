from spider.analysis.analysis import Analysis


class AnalysisTask(object):
    def __init__(self, uid, report_id):
        self.uid = uid
        self.report_id = report_id

    def __call__(self):
        self.crawler = Analysis(uid=self.uid, report_id=self.report_id)
        self.crawler.run()
        return self.uid

    def __str__(self):
        return 'start crawling %s' % self.uid
