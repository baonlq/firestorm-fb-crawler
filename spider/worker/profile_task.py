from spider.crawl.user import ProfileCrawler
from spider.worker.analysis_task import AnalysisTask


class ProfileTask(object):
    def __init__(self, uid, priority, report_id):
        self.uid = uid
        self.priority = priority
        self.report_id = report_id

    def __call__(self):
        self.crawler = ProfileCrawler(self.priority)
        self.crawler.start_crawling(self.uid)
        return AnalysisTask(uid=self.uid, report_id=self.report_id)

    def __str__(self):
        return 'start crawling %s' % self.uid
