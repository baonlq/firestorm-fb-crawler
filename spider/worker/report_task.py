from spider.crawl.user import ProfileCrawler
from spider.report.report_executor import ReportExecutor
from spider.worker.analysis_task import AnalysisTask


class ReportTask(object):
    def __init__(self, report_id, attachment):
        self.report_id = report_id
        self.attachment = attachment

    def __call__(self):
        self.crawler = ReportExecutor(self.report_id, attachment=self.attachment)
        self.crawler.start_crawl()
        return self.report_id

    def __str__(self):
        return 'start crawling %s' % self.report_id
