import threading
import time

import shared


class AntiSeederTask(threading.Thread):

    def __init__(self, group=None, target=None, name=None,
                 args=(), kwargs=None, verbose=None):
        threading.Thread.__init__(self, group=group, target=target, name=name,
                                  verbose=verbose)
        self.args = args
        self.kwargs = kwargs

    def count_free_worker(self):
        c = 0
        # print 'scanners: {}'.format(shared.scanners)
        for scanner in shared.scanners:
            if scanner['status'] == 'free':
                c += 1
        return c

    def scan(self, uid):
        print 'scanning {}'.format(uid)
        for worker in shared.scanners:
            if worker['status'] == 'free':
                worker['status'] = 'busy'
                res = worker['scanner'].scan_uid(uid=uid)
                worker['status'] = 'free'
                return res

    def run(self):
        # print 'scanners: {}'.format(shared.scanners)
        # print 'start with {}'.format(str(self.args))
        print '{} free workers'.format(self.count_free_worker())
        while self.count_free_worker() == 0:
            # print 'waiting for free worker'
            time.sleep(0.5)
        else:
            res = self.scan(self.args[0])

            if res:
                print 'add {}'.format(self.args[0])
                shared.scan_results.put(self.args[0])
                print 'completed {} profiles'.format(shared.scan_results.qsize())
                # self.args[1].put(self.args[0])
                return True
            return False
