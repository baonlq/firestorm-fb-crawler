# encoding=utf8
from datetime import date, datetime
import MySQLdb

import sys

reload(sys)
sys.setdefaultencoding('utf8')

from service.data_handler import DataHandler
from service.logger import b_log
from service.mysql_integration import MysqlIntegration


def _get_age(birthday):
    try:
        if birthday is not None:
            today = date.today()
            birthday = datetime.strptime(birthday, '%m/%d/%Y')
            return today.year - birthday.year - ((today.month, today.day) < (birthday.month, birthday.day))
        else:
            return 'unspecific'
    except Exception as e:
        b_log(e)
        return 'unspecific'


class Analysis:
    def __init__(self, uid, report_id):
        self.uid = uid
        self.user = None
        self.dtHandler = DataHandler()
        self.conn = self.dtHandler.get_connection()
        db = self.dtHandler.get_database()
        self.clt_users = db.get_collection('users')
        self.clt_pages = db.get_collection('pages')
        # self.clt_reports = self.db.get_collection('reports')
        # self.clt_location_reports = self.db.get_collection('location_reports')
        # self.clt_age_report = self.db.get_collection('age_reports')
        # self.clt_gender_report = self.db.get_collection('gender_reports')
        # self.clt_liked_page_report = self.db.get_collection('liked_page_reports')
        # self.clt_relationship_status_report = self.db.get_collection('relationship_status_reports')
        self.cursor = MysqlIntegration()
        self.report_id = report_id

    def _analysis_location(self):
        if self.user['location'] is not None:
            location = self.user['location']['id']
            # document = self.clt_location_reports.find_one(
            #     {'location.id': self.user['location']['id'], 'report_id': self.report_id})
        else:
            location = 'unspecific'
            # document = self.clt_location_reports.find_one({'location': 'unspecific', 'report_id': self.report_id})

        document = self.cursor.find_location_report(self.report_id, location)
        # print(document)

        if document is not None:
            self.cursor.update_count_report('location', document['id'])
            self.cursor.add_location_details(document['id'], self.uid)
        else:
            location_r = {
                'location_id': None,
                'location_name': None,
                'report_id': self.report_id,
                'count': 1
            }
            if location != 'unspecific':
                location_r['location_id'] = self.user['location']['id']
                location_r['location_name'] = str(self.user['location']['name']).encode('utf-8')
            else:
                location_r['location_id'] = 'unspecific'
            document_id = self.cursor.insert_location_report(location_r)
            self.cursor.add_location_details(document_id, self.uid)

    def _analysis_age(self):
        # document = self.clt_age_report.find_one({'age': _get_age(self.user['birthday']), 'report_id': self.report_id})
        document = self.cursor.find_age_report(_get_age(self.user['birthday']), self.report_id)
        if document is not None:
            self.cursor.update_count_report('age', document['id'])
            self.cursor.add_age_details(document['id'], self.uid)
        else:
            age_r = {
                'age': _get_age(self.user['birthday']),
                'count': 1,
                'report_id': self.report_id
            }
            age_id = self.cursor.insert_age_report(age_r)
            self.cursor.add_age_details(age_id, self.uid)

    def _analysis_gender(self):
        # document = self.clt_gender_report.find_one({'gender': self.user['gender'], 'report_id': self.report_id})
        gender = self.user['gender'] if self.user['gender'] is not None else 'unspecific'

        document = self.cursor.find_gender_report(gender, self.report_id)
        if document is not None:
            # self.clt_gender_report.update({'_id': document['_id']}, {'$set': {'count': document['count'] + 1}},
            #                               upsert=False)
            self.cursor.update_count_report('gender', document['id'])
            gender_id = document['id']
        else:
            gender_r = {
                'gender': gender,
                'count': 1,
                'report_id': self.report_id
            }
            # self.clt_gender_report.insert(gender_r)
            gender_id = self.cursor.insert_gender_report(gender_r)

        self.cursor.add_gender_details(gender_id, self.uid)

    def get_page_name(self, page_id):
        page = self.clt_pages.find_one({'id': page_id}, {'name': 1})
        if page is not None:
            return page['name']
        return None

    def _analysis_liked_page(self):
        for page_id in self.user['likes']:
            # document = self.clt_liked_page_report.find_one({'page': page_id, 'report_id': self.report_id})
            document = self.cursor.find_liked_page_report(page_id, self.report_id)
            if document is not None:
                # self.clt_liked_page_report.update({'_id': document['_id']}, {'$set': {'count': document['count'] + 1}},
                #                               upsert=False)
                self.cursor.update_count_report('pages', document['id'])
                page_id = document['id']
            else:
                page_r = {
                    'page': page_id,
                    'page_name': self.get_page_name(page_id),
                    'count': 1,
                    'report_id': self.report_id
                }
                # self.clt_liked_page_report.insert(page_r)
                page_id = self.cursor.insert_liked_page_report(page_r)
            self.cursor.add_page_details(page_id, self.uid)

    def _analysis_relationship_status(self):
        # document = self.clt_relationship_status_report.find_one({'relationship_status': self.user['relationship_status'], 'report_id': self.report_id})
        relationship_status = self.user['relationship_status'] if self.user[
                                                                      'relationship_status'] is not None else 'unspecific'
        document = self.cursor.find_relationship_status_report(relationship_status, self.report_id)
        if document is not None:
            # self.clt_relationship_status_report.update({'_id': document['_id']}, {'$set': {'count': document['count'] + 1}},
            #                               upsert=False)
            self.cursor.update_count_report('relationship_status', document['id'])
            relationship_id = document['id']
            # print 'add relationship_id {}'.format(relationship_id)
        else:
            relationship_r = {
                'relationship_status': relationship_status,
                'count': 1,
                'report_id': self.report_id
            }
            # self.clt_relationship_status_report.insert(gender_r)
            relationship_id = self.cursor.insert_relationship_status_report(relationship_r)
            # print 'update relationship_id {}'.format(relationship_id)
        self.cursor.add_relationship_details(relationship_id, self.uid)

    def run(self):
        for user in self.clt_users.find({'id': self.uid}):
            # b_log('got user {}'.format(user))
            self.user = user
            break

        if self.user is not None:
            self._analysis_location()
            self._analysis_age()
            self._analysis_gender()
            self._analysis_liked_page()
            self._analysis_relationship_status()
