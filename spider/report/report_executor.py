import shutil
from multiprocessing import JoinableQueue, Queue, cpu_count

import time

import requests

from crawler.worker.worker import Worker
from service.configs import api_endpoint, download_path
from service.data_handler import DataHandler
from service.logger import b_log
from service.mysql_integration import MysqlIntegration
from spider.anti.anti_seeder import AntiSeeder
from spider.worker.profile_task import ProfileTask


def download_file(url):
    local_filename = url.split('/')[-1]
    # NOTE the stream=True parameter
    try:
        r = requests.get(url, stream=True)
        with open(download_path + local_filename, 'wb') as f:
            for chunk in r.iter_lines():
                if chunk:  # filter out keep-alive new chunks
                    f.write(chunk)
                    f.write('\n')
                    # f.flush() commented by recommendation from J.F.Sebastian
        return download_path + local_filename
    except Exception as e:
        b_log('download file failed: {}'.format(e))


class ReportExecutor:
    def __init__(self, report_id, attachment):
        self.report_id = report_id
        self.attachment = attachment
        self.profile_tasks = JoinableQueue()
        self.crawl_results = JoinableQueue()
        self.analysis_results = Queue()
        self.number_workers = 20
        # self.number_workers = 1
        self.cursor = MysqlIntegration()

    def alert_email(self):
        r = requests.get(api_endpoint + '/report/alert')
        print(r.json())

    def start_crawl(self):
        try:
            b_log('Creating %d workers' % self.number_workers)
            workers = [Worker(self.profile_tasks, self.crawl_results)
                       for i in range(self.number_workers)]

            file_path = download_file(api_endpoint + self.attachment)
            file_path = open(file_path, 'r')
            number_of_uid = 0
            for l in file_path.readlines():
                if l is not None and not l == "":
                    number_of_uid += 1
                    # print(l.rstrip())
                    uid = l.rstrip()
                    self.profile_tasks.put(ProfileTask(uid=uid, priority=1, report_id=self.report_id))

            # uid_list = []
            # for l in file_path.readlines():
            #     uid = l.strip()
            #     if self.anti_seeder.scan_uid(uid):
            #         print '{} is real account'.format(uid)
            #         uid_list.append(uid)
            #     else:
            #         print '{} is seeding account, kill him!'.format(uid)

            # number_of_uid = len(uid_list)
            for w in workers:
                w.start()

            analysis_workers = [Worker(self.crawl_results, self.analysis_results)
                                for i in range(self.number_workers)]

            time.sleep(3)
            self.cursor.update_report_status('in-process', self.report_id)
            for w in analysis_workers:
                w.start()

            while True:
                print '{} < {}'.format(self.analysis_results.qsize(), number_of_uid)
                if self.analysis_results.qsize() == number_of_uid:
                    self.cursor.update_report_status('completed', self.report_id)
                    break
                time.sleep(1)

            for i in range(self.number_workers):
                self.profile_tasks.put(None)
                # self.crawl_results.put(None)

            self.profile_tasks.join()
        except Exception as e:
            b_log('ReportExecutor start_crawl error: {}'.format(e))
            self.cursor.update_report_status('pending', self.report_id)

