import time

from service.data_handler import DataHandler
from service.logger import b_log
from service.request_manager import RequestManager


class ProfileCrawler:
    def __init__(self, priority):
        self.requestManager = RequestManager()
        self.dtHandler = DataHandler()
        self.conn = self.dtHandler.get_connection()
        self.db = self.dtHandler.get_database()
        self.clt_users = self.db.get_collection('users')
        self.clt_pages = self.db.get_collection('pages')
        self.clt_groups = self.db.get_collection('groups')
        self.clt_comments = self.db.get_collection('comments')
        self.clt_feed = self.db.get_collection('feed')
        self.clt_token = self.db.get_collection('tokens')
        self.clt_places = self.db.get_collection('places')
        self.priority = priority

    def check_user_existed(self, uid):
        for record in self.clt_users.find({'id': uid}, {'id': 1}):
            return True
        return False

    def check_page_existed(self, page_id):
        for page in self.clt_pages.find({'id': page_id}, {'id': 1}):
            return True
        return False

    def check_group_existed(self, group_id):
        for group in self.clt_groups.find({'id': group_id}, {'id': 1}):
            return True
        return False

    def check_feed_existed(self, feed_id):
        for feed in self.clt_feed.find({'id': feed_id}, {'id': 1}):
            return True
        return False

    def check_comment_existed(self, cmt_id):
        for cmt in self.clt_comments.find({'id': cmt_id}, {'id': 1}):
            return True
        return False

    def get_requests(self, path, flag):
        response = self.requestManager.make_requests(path, flag)
        # print response
        # response = [] if response is None else response
        return response

    def _get_pages(self, pages, page_list):
        if pages is None:
            # logging.warn('pages is {}'.format(pages))
            return page_list

        # print('found pages: {}'.format(len(pages.get('data'))))
        if 'data' in pages:
            for page in pages.get('data'):
                page_list.append(page)
                page_record = {
                    'id': page.get('id'),
                    'name': page.get('name'),
                    'priority': self.priority
                }
                if not self.check_page_existed(page.get('id')):
                    self.clt_pages.insert(page_record)
                    # self.producer.publish('insert', 'pages', page_record)

            if 'paging' in pages and 'next' in pages['paging']:
                pages_url = pages['paging']['next']
                page_list = self._get_pages(self.get_requests(pages_url, 'full'), page_list)

        return page_list

    def get_user_likes(self, uid):
        # get all pages this person has liked
        execution_start_time = time.time()
        b_log('Start getting likes processing for user {}'.format(uid))
        likes_url = uid + '/likes/?fields=id,name&limit=100'
        likes_list = self._get_pages(self.get_requests(likes_url, 'path'), [])
        # print 'likes_list {}'.format(likes_list)

        pages = []
        for page in likes_list:
            pages.append(page.get('id'))

        # insert pages list to self.clt_users
        self.clt_users.update({'id': uid}, {'$set': {'likes': pages}}, upsert=False)

        cost_time = time.time() - execution_start_time
        b_log('Complete getting likes processing. Time cost: {}'.format(cost_time))
        return True

    def get_user_information(self, uid):

        execution_start_time = time.time()
        b_log('Start get user information {}'.format(uid))

        me_response = self.get_requests(
            uid + '?fields=about,birthday,name,cover,devices,education,email,gender,hometown,interested_in,location,relationship_status,work','path')

        if me_response is None:
            return False
        elif me_response == 'type_page':
            b_log('{} is not user, moving to pages'.format(uid))
            user = self.clt_users.find_one({'id': uid}, {'id': 1})
            self.clt_users.remove({'id': uid})
            if not self.check_page_existed(uid):
                self.clt_pages.insert(user)
                # self.producer.publish('insert', 'pages', user)
            return False

        # print me_response
        user_record = {
            'id': me_response.get('id'),
            'about': me_response.get('about'),
            'name': me_response.get('name'),
            'birthday': me_response.get('birthday'),
            'cover': me_response.get('cover'),
            'currency': me_response.get('currency'),
            'devices': me_response.get('devices'),
            'education': me_response.get('education'),
            'email': me_response.get('email'),
            'gender': me_response.get('gender'),
            'hometown': me_response.get('hometown'),
            'interested_in': me_response.get('interested_in'),
            'location': me_response.get('location'),
            'relationship_status': me_response.get('relationship_status'),
            'work': me_response.get('work'),
            'priority': 2,
            'likes': []
        }
        # insert user to db
        if not self.check_user_existed(uid):
            b_log('inserted new user {}'.format(uid))
            self.clt_users.insert(user_record)
        else:
            b_log('update user {}'.format(uid))
            self.clt_users.update({'id': me_response.get('id')}, user_record, upsert=False)

        cost_time = time.time() - execution_start_time
        b_log('Getting user {} information completed. Time cost: {}'.format(uid, cost_time))
        return True

    def start_crawling(self, uid):
        b_log("Start crawling profile {} process".format(uid))
        self.get_user_information(uid)
        self.get_user_likes(uid)
