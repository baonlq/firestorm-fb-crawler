import time
from multiprocessing import JoinableQueue, cpu_count, Queue

from crawler.worker.worker import Worker
from service.configs import api_endpoint
from service.data_handler import DataHandler
from service.logger import b_log
from service.mysql_integration import MysqlIntegration
from spider.report.report_executor import download_file
from spider.worker.profile_task import ProfileTask
from spider.worker.report_task import ReportTask


class Executor:
    def __init__(self):
        self.tasks = JoinableQueue()
        self.results = Queue()
        self.number_workers = 20
        # self.number_workers = 3
        self.dtHandler = DataHandler()
        self.conn = self.dtHandler.get_connection()
        self.db = self.dtHandler.get_database()
        self.clt_reports = self.db.get_collection('reports')
        self.cursor = MysqlIntegration()

    def execute(self):
        reports = self.cursor.get_all_pending_report()
        if len(reports) == 0:
            b_log('ERROR! No pending reports!')
            return False

        b_log('Creating %d workers' % self.number_workers)
        workers = [Worker(self.tasks, self.results)
                   for i in range(self.number_workers)]

        for w in workers:
            w.start()

        for report in reports:
            # scan first
            file_path = download_file(api_endpoint + report['attachment'])
            file_path = open(file_path, 'r')

            # put task
            self.tasks.put(
                ReportTask(report_id=report['id'], attachment=report['attachment']))

        for i in range(self.number_workers):
            self.tasks.put(None)
