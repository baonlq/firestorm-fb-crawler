import json
from kafka import KafkaProducer

from service.logger import b_log


class Producer:
    daemon = True

    def __init__(self, host, port):
        b_log('Establishing connection to Kafka')
        self.producer = KafkaProducer(bootstrap_servers=host + ':' + port)

    def _publish_message(self, message):
        try:
            self.producer.send('incoming', json.dumps(message))
            # block until all async messages are sent
            self.producer.flush()
            # configure multiple retries
            # self.producer = KafkaProducer(retries=5)
        except Exception as e:
            b_log('Got an error when submit to topic incoming {}'.format(e))
            pass

    def publish(self, action, collection, data):
        try:
            if collection is None:
                b_log("Collection is invalid")
                return

            if data is None:
                b_log("Data is None, nothing to publish")
                return

            if action == 'insert':
                msg = {
                    "type": "mongo",
                    "detail": {
                        "collection": collection,
                        "action": "insert",
                        "document": data
                    }
                }
            elif action == 'update':
                msg = {
                    "type": "mongo",
                    "detail": {
                        "collection": collection,
                        "action": "update",
                        "query": data['query'],
                        "update": data['update_fields'],
                        "options": data['options'] if 'options' in data else None
                    }
                }
            elif action == 'remove':
                msg = {
                    "type": "mongo",
                    "detail": {
                        "collection": collection,
                        "action": "remove",
                        "query": data['query'],
                        "options": data['options'] if 'options' in data else None
                    }
                }
            else:
                b_log("publish message without specify action")
                return

            self._publish_message(msg)
        except Exception as e:
            b_log('cannot publish message: {}'.format(e))
