#!/usr/bin/env python
# -*- coding: utf-8 -*-
import time

from spider.anti.scanner import BrowserManager
from spider.executor import Executor

if __name__ == '__main__':
    while True:
        exe = Executor()
        exe.execute()
        time.sleep(1*60)
