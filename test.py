from multiprocessing import freeze_support, Queue

import shared
from spider.anti.anti_seeder import AntiSeeder

# anti_seeder = AntiSeeder()
# anti_seeder.log_in()

# uid = 'thao.dangphuong.3'
# if anti_seeder.scan_uid(uid):
#     print '{} is real account'.format(uid)
# else:
#     print '{} is seeding account, kill him!'.format(uid)

# file_path = open('data/result-1.txt', 'r')
# for l in file_path.readlines():
#     uid = l.strip()
#     if anti_seeder.scan_uid(uid):
#         print '{} is real account'.format(uid)
#     else:
#         print '{} is seeding account, kill him!'.format(uid)
# anti_seeder.reset()
from spider.anti.browser_manager import BrowserManager
from spider.anti.scanner import ScanningExecutor

if __name__ == '__main__':
    m = BrowserManager()
    m.create_browsers()

    freeze_support()
    shared.scanners = []
    shared.scan_results = Queue()
    for scanner in m.get_scanners():
        scanner['scanner'].log_in()
        shared.scanners.append(scanner)

    scan_task = ScanningExecutor()
    file_path = open('data/result-1.txt', 'r')

    for l in file_path.readlines():
        if l is not None and not l == "":
            # print(l.rstrip())
            uid = l.rstrip()
            # print 'adding uid {}'.format(uid)
            scan_task.add_uid(uid)
    scan_task.start_scan()
    print 'tao bi khung roi'
