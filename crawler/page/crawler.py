import time
from datetime import timedelta, datetime

from multiprocessing import cpu_count, JoinableQueue
from queue import Queue

from crawler.worker.feed_task import FeedTask
from crawler.worker.worker import Worker
from service.data_handler import DataHandler
from service.logger import b_log
from service.request_manager import RequestManager


class PageCrawler:
    def __init__(self, priority):
        self.requestManager = RequestManager()
        self.dtHandler = DataHandler()
        self.conn = self.dtHandler.get_connection()
        self.db = self.dtHandler.get_database()
        self.clt_users = self.db.get_collection('users')
        self.clt_pages = self.db.get_collection('pages')
        self.clt_groups = self.db.get_collection('groups')
        self.clt_comments = self.db.get_collection('comments')
        self.clt_feed = self.db.get_collection('feed')
        self.clt_token = self.db.get_collection('tokens')
        # self.producer = Producer('localhost', '9092')
        self.priority = priority
        self.number_workers = cpu_count()
        self.feed_tasks = JoinableQueue()
        self.feed_results = Queue()

    def check_user_existed(self, uid):
        try:
            for record in self.clt_users.find({'id': uid}, {'id': 1}):
                return True
            return False
        except Exception as e:
            b_log('Exception: {}'.format(e))

    def check_page_existed(self, page_id):
        try:
            for page in self.clt_pages.find({'id': page_id}, {'id': 1}):
                return True
            return False
        except Exception as e:
            b_log('Exception: {}'.format(e))

    def check_group_existed(self, group_id):
        try:
            for group in self.clt_pages.find({'id': group_id}, {'id': 1}):
                return True
            return False
        except Exception as e:
            b_log('Exception: {}'.format(e))

    def check_feed_existed(self, feed_id):
        try:
            for feed in self.clt_feed.find({'id': feed_id}, {'id': 1}):
                return True
            return False
        except Exception as e:
            b_log('Exception: {}'.format(e))

    def get_requests(self, path, flag):
        response = self.requestManager.make_requests(path, flag)
        # print response
        # response = [] if response is None else response
        return response

    def _get_feed_ids(self, feeds, feed_list):
        try:
            # print feeds
            if feeds is None:
                return feed_list
            # print 'get {} feeds'.format(len(feeds['data']))
            if 'data' in feeds:
                for feed in feeds.get('data'):
                    feed_list.append(feed)
                    feed_record = {
                        'id': feed.get('id'),
                        'message': feed.get('message'),
                        'link': feed.get('link'),
                        'story_tags': feed.get('story_tags'),
                        'created_time': feed.get('created_time'),
                        'priority': self.priority,
                        'author': None
                    }
                    self.clt_feed.update({'id': feed.get('id')}, feed_record, upsert=True)
                    # self.producer.publish('update', 'feed', {
                    #     'query': {'id': feed.get('id')},
                    #     'update_fields': feed_record,
                    #     'options': {'upsert': True}
                    # })

            if 'paging' in feeds and 'next' in feeds['paging']:
                feeds_url = feeds['paging']['next']
                feed_list = self._get_feed_ids(self.get_requests(feeds_url, 'full'), feed_list)

            return feed_list
        except Exception as e:
            b_log('Exception: {}'.format(e))

    def _start_feed_process(self):
        b_log('Creating {} workers to crawl feed'.format(self.number_workers))
        workers = [Worker(self.feed_tasks, self.feed_results)
                   for i in range(self.number_workers)]
        for w in workers:
            w.start()

        self.feed_tasks.join()

    def get_page_feed(self, uid):
        try:
            execution_start_time = time.time()
            b_log('Start feed processing for page {}'.format(uid))
            # get all pages this person has liked
            d = datetime.today() - timedelta(days=90)
            since = d.strftime('%d-%m-%Y')
            feed_url = uid + '/feed/?limit=100&since=' + since
            feed_list = self._get_feed_ids(self.get_requests(feed_url, 'path'), [])

            # Get message, comments and reactions from feed.
            # using multiprocess
            self._start_feed_process()

            page_feeds = []
            for f in feed_list:
                # self.producer.publish('update', 'feed', {
                #     'query': {'id': f.get('id')},
                #     'update_fields': {'author': {
                #         'type': 'page',
                #         'id': uid
                #     }},
                #     'options': {
                #         'upsert': False
                #     }
                # })
                page_feeds.append(f.get('id'))
                self.feed_tasks.put(FeedTask(f.get('id')))

            self.clt_pages.update({'id': uid}, {'$set': {'feeds': page_feeds}}, upsert=False)
            # self.producer.publish('update', 'pages', {
            #     'query': {'id': uid},
            #     'update_fields': {'feeds': page_feeds},
            #     'options': {'upsert': False}
            # })

            cost_time = time.time() - execution_start_time
            b_log('Complete feed processing. Time cost: {}'.format(cost_time))
            return True
        except Exception as e:
            b_log('Exception: {}'.format(e))

    def get_page_information(self, uid):
        try:
            execution_start_time = time.time()
            b_log('Start getting page information processing for {}'.format(uid))

            response = self.get_requests(
                uid + '?fields=about,can_checkin,category,checkins,contact_address,cover,description,emails,fan_count,location,name,phone,rating_count,start_info',
                'path')
            # print('response: {}'.format(response))
            if response is None:
                return False

            if 'id' in response:
                page_record = {
                    'id': response.get('id'),
                    'can_checkin': response.get('can_checkin'),
                    'category': response.get('category'),
                    'checkins': response.get('checkins'),
                    'cover': response.get('cover'),
                    'fan_count': response.get('fan_count'),
                    'name': response.get('name'),
                    'phone': response.get('phone'),
                    'rating_count': response.get('rating_count'),
                    'start_info': response.get('start_info'),
                    'priority': 2
                }
                # print page_record
                # insert user to db
                # b_log('got page information: {}'.format(response))
                if not self.check_page_existed(page_id=response.get('id')):
                    self.clt_pages.insert_one(page_record)
                else:
                    self.clt_pages.update({'id': response.get('id')}, page_record, upsert=True)
                # self.producer.publish('update', 'pages', {
                #     'query': {'id': response.get('id')},
                #     'update_fields': page_record,
                #     'options': {'upsert': True}
                # })
                cost_time = time.time() - execution_start_time
                b_log('Complete getting page information processing. Time cost: {}'.format(cost_time))
                return True
            return False
        except Exception as e:
            b_log('Exception: {}'.format(e))

    def start_crawling(self, uid):
        b_log('Start crawling page {} processing'.format(uid))
        try:
            self.get_page_information(uid)
            self.get_page_feed(uid)
            self.conn.close()
        except Exception as e:
            b_log('Exception: {}'.format(e))
