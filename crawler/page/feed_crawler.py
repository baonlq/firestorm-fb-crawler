import time

from service.data_handler import DataHandler
from service.logger import b_log
from service.request_manager import RequestManager


class FeedCrawler:
    def __init__(self):
        self.requestManager = RequestManager()
        self.dtHandler = DataHandler()
        self.conn = self.dtHandler.get_connection()
        self.db = self.dtHandler.get_database()
        self.clt_users = self.db.get_collection('users')
        self.clt_pages = self.db.get_collection('pages')
        self.clt_groups = self.db.get_collection('groups')
        self.clt_comments = self.db.get_collection('comments')
        self.clt_feed = self.db.get_collection('feed')
        self.clt_token = self.db.get_collection('tokens')
        self.clt_places = self.db.get_collection('places')
        # self.producer = Producer('173.208.199.99', '9094')

    def get_requests(self, path, flag):
        try:
            response = self.requestManager.make_requests(path, flag)
            # print response
            # response = [] if response is None else response
            return response
        except Exception as e:
            b_log('Exception: {}'.format(e))

    def check_comment_existed(self, cmt_id):
        try:
            for cmt in self.clt_comments.find({'id': cmt_id}, {'id': 1}):
                return True
            return False
        except Exception as e:
            b_log('Exception: {}'.format(e))

    def check_user_existed(self, uid):
        try:
            for record in self.clt_users.find({'id': uid}, {'id': 1}):
                return True
            return False
        except Exception as e:
            b_log('Exception: {}'.format(e))

    def _get_comments(self, comments, comments_list):
        try:
            execution_start_time = time.time()
            b_log('Start getting feed comments processing')

            if comments is None:
                return comments_list

            # If comments exist.
            comments = comments['comments'] if 'comments' in comments else comments
            # print comments
            if 'data' in comments:
                for comment in comments['data']:
                    comment_content = {
                        'id': comment['id'],
                        'created_by': comment['from']['id'],
                        'message': comment['message'],
                        'like_count': comment['like_count'] if 'like_count' in comment else None,
                        'created_time': comment['created_time'],
                        'author': comment['from']['id']
                    }
                    cmt_author = {
                        'id': comment['from']['id'],
                        'name': comment['from']['name'] if 'name' in comment['from'] else None,
                        'priority': 1
                    }
                    # self.clt_comments.insert_one(comment_content)
                    if not self.check_comment_existed(comment_content['id']):
                        # self.producer.publish('insert', 'comments', comment_content)
                        self.clt_comments.insert_one(comment_content)
                    comments_list.append(comment_content)
                    if not (self.check_user_existed(cmt_author['id'])):
                        self.clt_users.insert_one(cmt_author)
                        # self.producer.publish('insert', 'users', cmt_author)

                # Check comments has next or not.
                if 'paging' in comments:
                    if 'next' in comments.get('paging'):
                        comments_url = comments['paging']['next']
                        comments_list = self._get_comments(self.get_requests(comments_url, 'full'), comments_list)

            cost_time = time.time() - execution_start_time
            b_log('Completed getting feed comments processing. Time cost: {}'.format(cost_time))
            return comments_list
        except Exception as e:
            b_log('Exception: {}'.format(e))

    def _get_reactions(self, reactions, reactions_count_dict):
        try:
            execution_start_time = time.time()
            b_log('Start getting feed reactions processing')

            if reactions is None:
                return reactions_count_dict

            # If reactions exist.
            reactions = reactions['reactions'] if 'reactions' in reactions else reactions
            if 'data' in reactions:
                for reaction in reactions['data']:
                    reactor = {
                        'id': reaction.get('id'),
                        'name': reaction.get('name') if 'name' in reaction else None,
                        'priority': 1
                    }

                    if reaction['type'] == 'LIKE':
                        reactions_count_dict['like'].append(reactor.get('id'))
                    elif reaction['type'] == 'LOVE':
                        reactions_count_dict['love'].append(reactor.get('id'))
                    elif reaction['type'] == 'HAHA':
                        reactions_count_dict['haha'].append(reactor.get('id'))
                    elif reaction['type'] == 'WOW':
                        reactions_count_dict['wow'].append(reactor.get('id'))
                    elif reaction['type'] == 'SAD':
                        reactions_count_dict['sad'].append(reactor.get('id'))
                    elif reaction['type'] == 'ANGRY':
                        reactions_count_dict['angry'].append(reactor.get('id'))

                    if not (self.check_user_existed(reactor.get('id'))):
                        self.clt_users.insert_one(reactor)
                        # self.producer.publish('insert', 'users', reactor)

                # Check reactions has next or not.
                if 'next' in reactions['paging']:
                    reactions_url = reactions['paging']['next']
                    reactions_count_dict = self._get_reactions(self.get_requests(reactions_url, 'full'),
                                                               reactions_count_dict)

            cost_time = time.time() - execution_start_time
            b_log('Completed getting feed reactions processing. Time cost: {}'.format(cost_time))
            return reactions_count_dict
        except Exception as e:
            b_log('Exception: {}'.format(e))

    @staticmethod
    def _get_attachments(attachments, attachments_content):
        execution_start_time = time.time()
        b_log('Start getting feed attachments processing')

        if attachments is None:
            return attachments_content
        # If attachments exist.
        attachments = attachments['attachments'] if 'attachments' in attachments else attachments
        if 'data' in attachments:
            attachments_content['title'] = attachments['data'][0]['title'] if 'title' in attachments['data'][0] else ''
            attachments_content['description'] = attachments['data'][0]['description'] if 'description' in \
                                                                                          attachments['data'][0] else ''
            attachments_content['target'] = attachments['data'][0]['target']['url'] if 'target' in attachments['data'][
                0] and 'url' in attachments['data'][0]['target'] else ''

        cost_time = time.time() - execution_start_time
        b_log('Completed getting feed attachments processing. Time cost: {}'.format(cost_time))
        return attachments_content

    def _get_feed(self, feed):
        try:
            execution_start_time = time.time()
            b_log('Start get feed {} details'.format(feed['id']))
            # For comments.
            comments_url = feed.get('id') + '?fields=comments.limit(100)'
            # print 'comments url: {}'.format(comments_url)
            comments_list = self._get_comments(self.get_requests(comments_url, 'path'), [])
            comments_record = []
            for comment in comments_list:
                comments_record.append(comment.get('id'))

            self.clt_feed.update({'id': feed.get('id')}, {'$set': {'comments': comments_record}}, upsert=False)
            # self.producer.publish('update', 'feed', {
            #     'query': {'id': feed.get('id')},
            #     'update_fields': {'comments': comments_record},
            #     'options': {'upsert': False}
            # })

            # For reactions.
            reactions_count_dict = {
                'like': [],
                'love': [],
                'haha': [],
                'wow': [],
                'sad': [],
                'angry': []
            }
            reactions_url = feed.get('id') + '?fields=reactions.limit(100)'
            reactions_count_dict = self._get_reactions(self.get_requests(reactions_url, 'path'), reactions_count_dict)
            self.clt_feed.update({'id': feed.get('id')}, {'$set': {'reactions': reactions_count_dict}}, upsert=False)
            # self.producer.publish('update', 'feed', {
            #     'query': {'id': feed.get('id')},
            #     'update_fields': {'reactions': reactions_count_dict},
            #     'options': {'upsert': False}
            # })

            # For attachments.
            attachments_content = {
                'title': '',
                'description': '',
                'target': ''
            }
            attachments_url = feed.get('id') + '?fields=attachments'
            attachments_content = self._get_attachments(self.get_requests(attachments_url, 'path'), attachments_content)
            self.clt_feed.update({'id': feed.get('id')}, {'$set': {'attachments': attachments_content}}, upsert=False)
            # self.producer.publish('update', 'feed', {
            #     'query': {'id': feed.get('id')},
            #     'update_fields': {'attachments': attachments_content},
            #     'options': {'upsert': False}
            # })

            # uppate crawled status
            self.clt_feed.update({'id': feed.get('id')}, {'$set': {'priority': 2}}, upsert=False)
            # self.producer.publish('update', 'feed', {
            #     'query': {'id': feed.get('id')},
            #     'update_fields': {'priority': 2},
            #     'options': {
            #         'upsert': False
            #     }
            # })
            cost_time = time.time() - execution_start_time
            b_log('Getting feed {} details completed. Time cost: {}'.format(feed['id'], cost_time))
        except Exception as e:
            b_log('Exception: {}'.format(e))

    def start_crawling(self, feed_id):
        try:
            self._get_feed({'id': feed_id})
            self.conn.close()
        except Exception as e:
            b_log('Exception: {}'.format(e))
