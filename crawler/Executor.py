from multiprocessing import JoinableQueue
from queue import Queue

from crawler.worker.page_task import PageTask
from crawler.worker.worker import Worker
from service.data_handler import DataHandler
from service.logger import b_log


class Executor:
    def __init__(self):
        # Establish communication queues
        self.tasks = JoinableQueue()
        self.results = Queue()
        self.dbHandler = DataHandler()
        self.number_workers = 2
        self.steps = 2

    def _execute(self, p=0):
        conn = self.dbHandler.get_connection()
        db = self.dbHandler.get_database()

        while True:
            documents = self._get_documents(db, p)

            for document in documents:
                db.get_collection('pages').update({'id': document.get('id')}, {'$set': {'priority': 3}}, upsert=False)
                uid = document.get('id')
                self.tasks.put(PageTask(uid, document.get('priority')))
            p += 1

    def execute(self):
        b_log('Creating %d workers' % self.number_workers)
        workers = [Worker(self.tasks, self.results)
                   for i in range(self.number_workers)]
        for w in workers:
            w.start()

        self._execute()
        # Causes the main thread to wait for the queue to finish processing all the tasks
        self.tasks.join()

    def _get_documents(self, db, skip_num):
        documents = db.get_collection('pages').find({}, {'id': 1, 'priority': 1}).skip(self.steps * skip_num).limit(
            self.steps).sort('priority', 1)
        return documents
