from crawler.page.feed_crawler import FeedCrawler


class FeedTask(object):
    def __init__(self, uid):
        self.uid = uid

    def __call__(self):
        self.crawler = FeedCrawler()
        self.crawler.start_crawling(self.uid)
        return self.uid

    def __str__(self):
        return 'start crawling %s' % self.uid
