from multiprocessing import Process, Queue

from service.logger import b_log


class Worker(Process):
    def __init__(self, task_queue, result_queue):
        Process.__init__(self)
        self.task_queue = task_queue
        self.result_queue = result_queue

    def run(self):
        proc_name = self.name
        while True:
            next_task = self.task_queue.get()
            if next_task is None:
                # Poison pill means shutdown
                b_log('Finished {}'.format(proc_name))
                self.task_queue.task_done()
                break
            answer = next_task()
            print('answer: {}'.format(answer))
            self.task_queue.task_done()
            self.result_queue.put(answer)
            b_log('completed: {} items'.format(self.result_queue.qsize()))
        return
