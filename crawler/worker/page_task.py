from crawler.page.crawler import PageCrawler


class PageTask(object):
    def __init__(self, uid, priority):
        self.uid = uid
        self.priority = priority

    def __call__(self):
        self.crawler = PageCrawler(self.priority)
        self.crawler.start_crawling(self.uid)
        return self.uid

    def __str__(self):
        return 'start crawling %s' % self.uid
