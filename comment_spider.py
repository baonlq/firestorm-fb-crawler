import time

from service.request_manager import RequestManager

requestManager = RequestManager()


def get_requests(path, flag):
    response = requestManager.make_requests(path, flag)
    return response


def _get_comments(comments, authors):
    execution_start_time = time.time()
    print('Start getting feed comments processing')

    if comments is None:
        return authors

    # If comments exist.
    comments = comments['comments'] if 'comments' in comments else comments
    # print comments
    if 'data' in comments:
        for comment in comments['data']:
            author_id = comment['from']['id']
            if author_id not in authors:
                authors.append(author_id)

        # Check comments has next or not.
        if 'paging' in comments:
            if 'next' in comments.get('paging'):
                comments_url = comments['paging']['next']
                authors = _get_comments(get_requests(comments_url, 'full'), authors)

    cost_time = time.time() - execution_start_time
    print('Completed getting feed comments processing. Time cost: {}'.format(cost_time))
    return authors


def get_comments(p_id):
    # For comments.
    comments_url = p_id + '/comments?limit=100'
    authors = _get_comments(get_requests(comments_url, 'path'), [])
    f = open('data/hismile-cmt-2011.csv', 'w')
    for uid in authors:
        f.write('{}\n'.format(uid))


if __name__ == '__main__':
    p_id = '1573441899601646_2045362455742919'
    get_comments(p_id)
